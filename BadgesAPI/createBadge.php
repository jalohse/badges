<?php

//check for https
//md5
//setcookie

error_reporting(E_ALL);
ini_set('display_errors', '1');


/**
 * @param $name string
 * @return array
 */
function param_as_file_data($name)
{
    (array_key_exists($name, $_FILES) AND strlen($_FILES[$name]['name']) > 0)
        OR
        header("HTTP/1.1 400 Bad Request");

    $nameParts = explode('.', $_FILES[$name]['name']);

    return array(
        'contents' => file_get_contents($_FILES[$name]['tmp_name']),
        'extension' => strtolower($nameParts[count($nameParts) - 1])
    );
}

if(isset($_POST['name'])){
    $file = param_as_file_data('image');
    $signature = sha1($file['contents']);

    $validExtensions = array('png');

    if (in_array($file['extension'], $validExtensions)) {

        $object = array();
        $fileName = str_replace(' ','_',$_POST['name']);
        print_r($fileName);
        $object["criteria"] =  'http://web.et.bsu.edu/2014/jessica/php/criteria/'.$fileName."_criteria.txt";
        $object["name"]= $_POST["name"];
        $object["description"] = $_POST["description"];
        $object["issuer"] = "http://web.et.bsu.edu/2014/jessica/issuer.json";
        var_dump($object);
        // Save the image to disk
        $pathToData = "images/";
        $imageFilePath = "{$pathToData}{$signature}.{$file['extension']}";
        if(!file_put_contents($imageFilePath, $file['contents'])){
            echo "failed to save image contents";
        }

        $object["image"] = "http://web.et.bsu.edu/2014/jessica/php/images/".$signature.".".$file['extension'];

        // Build the web URL for the image... additionally build it protocol-less.
        $imageUrlPrefix = str_replace('http://', '//', 'https://web.et.bsu.edu/2014/jessica/php');
        $imageUrlPrefix = str_replace('https://', '//', $imageUrlPrefix);
        $imageUrl = "{$imageUrlPrefix}data/{$signature}.{$file['extension']}";

        // Usr $imageUrl


        //var_dump($_POST['criteria']);
        if(!file_put_contents("criteria/".$fileName."_criteria.txt",$_POST['criteria'])){
            echo "failed to make criteria";
        }
        $_POST['criteria'] = $imageUrlPrefix.'/criteria/'.$fileName."_criteria.txt";
        $_POST['image'] = $imageUrlPrefix.'/'.$imageFilePath; //secure? not now
        if(!file_put_contents("badges/".$fileName.".txt",json_encode($object))){
            echo "failed to save badge";
        }
    }
}


?>

<html>
<form action="createBadge.php" enctype="multipart/form-data" method="post">
    <div>
        <label for="name">Name of Badge:</label>
        <input type="text" name="name" id="name"/>
    </div>
    <div>
        <label for="image">Image:</label>
        <input type="file" name="image" id="image"/>
    </div>
    <div>
        <label for="description">Badge Description:</label>
        <input type="text" name="description" id="description"/>
    </div>
    <div>
        <label for="criteria">Badge Criteria:</label>
        <textarea cols="80" rows="15" name="criteria" id="criteria"> </textarea>
    </div>
    <div>
        <input type="submit" value="Make Badge"/>
    </div>
</form>

</html>