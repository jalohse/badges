<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jessicalohse1
 * Date: 1/14/14
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */

//make assertions
unset($_POST['submit']);
if (isset($_POST['email']) && isset($_POST['badgeURL'])) {
    // Need to sanitize and validate the data
    /*
     * There should also be a check to prevent duplication of badges.
     * Without a backend this might be best accomplished with a CSV file containing the hashed emails
     * that have already been granted badges.
     */
    $numberOfAssertions = count(scandir("assertions"));
    $assertion = array();
    $assertionId = $numberOfAssertions+1;
    $assertion['uid'] = (string)$assertionId;
    $assertion['recipient'] = array();
    $assertion['recipient']['type'] = "email";
    $assertion['recipient']['hashed'] = false;
    $assertion['recipient']['identity'] = $_POST['email'];
    $assertion['issuedOn'] = time();
    $assertion['badge'] = "http://web.et.bsu.edu/2014/jessica/php/badges/".$_POST['badgeURL'];
    $assertion['verify'] = array();
    $assertion['verify']['type'] = "hosted";
    $assertion['verify']['url'] = "http://web.et.bsu.edu/2014/jessica/php/assertions/".$assertion['uid'].".json";
    $assertion = json_encode($assertion);
    $assertionUrl = $assertionId.".json";
    file_put_contents("assertions/".$assertionUrl, $assertion);
    echo $assertionUrl;
    die();
}
?>

<html>
<form method="post">
    <div>
        <label for="email">User's email:</label>
        <input type="text" name="email" id="email"/>
    </div>
    <div>
        <label for="badgeURL">Badge URL:</label>
        <select name="badgeURL" id="badgeURL">
            <?php
            $filenames = scandir("badges");
            foreach ($filenames as $file) {
                echo "<option value=" . $file . ">" . $file . "</option>";
            }
            ?>
    </div>
    <div>
        <input name="submit" type="submit" value="Award Badge"/>
    </div>
</form>

</html>